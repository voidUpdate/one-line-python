# Minimalist Python

These are some scripts that I write for fun, and try to [golf](https://codegolf.stackexchange.com/) as much as possible. They will usually be badly written, but do their job

* fizzbuzz: A one line script that performs fizzbuzz on the first 100 numbers
* word scrambulator: A two line script that takes each word in the input, and scrambles all the letters except for the first and last. Based off [this study](https://www.mrc-cbu.cam.ac.uk/people/matt.davis/Cmabrigde/). There is a slight bug where a single letter will becomed duplicated, such as "a" becoming "aa"
* recaman: 3-line script that generates the recaman sequence

